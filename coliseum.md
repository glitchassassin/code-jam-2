## Coliseum

### Setup

1. Install MongoDB
2. Install pip requirements

### Demo

1. Drop the `codejam` table from MongoDB to reinitialize (if necessary)
2. Run `python -m tests`