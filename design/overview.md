Coliseum is a combat simulator for mythological heroes.

The API supports the following actions:

1. Add a hero
2. Modify a hero (?)
3. List all available heroes
4. Declare a Fight
5. Get the results of a specific fight
6. Get a hero's performance stats

A Hero is described by:

1. ID (user provided)
2. Name
3. Description
4. List of Powers

A Power is described by:

1. Number of uses (1-100)
2. Potential strength (1-100)

A Fight is defined by:

1. A list of Heroes

A Fight's Results include:

1. The list of Heroes that fought
2. For each hero, their most effective and least effective power
3. For each hero, their total score

To calculate a Hero's score:

1. For each of the Hero's powers:
2. For each use of the power:
3. Roll 1d[power_strength]
4. The total of the above is the Hero's Score.

The highest score is the winner.
