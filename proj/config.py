import json
import os

# Default "just in case" configuration
DEFAULT = {
    "mongourl": "mongodb://127.0.0.1:27017",
    "database": "codejam"
}


def get_config():
    """Open up a config.json file, and if it does not exist use default values."""
    # Check if the config file exists, if not just return the default
    if not os.path.isfile("config.json"):
        return DEFAULT

    # At this point we asume that the file indeed exists, so let's open it up
    with open("config.json") as file:
        # Create a copy of the default values...
        cfg = DEFAULT
        # ...and add the ones from the JSON file
        cfg.update(json.load(file))

    # To end, just return the new values
    return cfg
