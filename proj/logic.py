"""The application logic for Coliseum is contained here.

This includes calculating scores, running fights, etc.
"""

from random import randint


def do_fight(heroes):
    """
    Calculates the outcome of a fight between all the heroes
    provided in the `heroes` list. Returns the results of the
    fight as a structured dict.

    Parameters:
    heroes -- A list of Hero objects

    Returns:
    {
        winner: A dict representing the individual performance of the winner
        heroes: A list representing the individual performance of each hero
        [{
            hero: The Hero object
            score: The final score for the Hero
            best_power: The hero's most effective power this round
            (
                [0]: The Power object
                [1]: The amount of Score contributed by this power
            )
            worst_power: The hero's least effective power this round
            (
                [0]: The Power object
                [1]: The amount of Score contributed by this power
            )
        }]
    }
    """
    results = {
        "winner": None,
        "heroes": []
    }
    for hero in heroes:
        # A power's attack is a random number between 1 and its strength.
        # Each power is used the maximum number of times.
        powers = [{
            "power": p,
            "score": sum([randint(1, p["strength"]) for attack in range(p["uses"])])
        } for p in hero["powers"]]

        # Sort the powers by the total attack contributed
        powers.sort(key=lambda x: x["score"])
        score = sum([p["score"] for p in powers])
        results["heroes"].append({
            "hero": hero,
            "score": score,
            "best_power": powers[-1],
            "worst_power": powers[0]
        })
    results["heroes"].sort(key=lambda x: x["score"])
    results["winner"] = results["heroes"][-1]
    return results


def get_stats(fights, hero_id):
    """
    Calculates a hero's performance across a list of Fight Results.

    Parameters:
    fights -- A list of Fight Result objects.
    hero_id -- The hero for whom to compile stats

    Returns:
    {
        fights: <int>,
        wins: <int>,
        losses: <int>,
        highest_score: <int>,
        lowest_score: <int>
    }
    """
    hero_stats = {
        "fights": 0,
        "wins": 0,
        "losses": 0,
        "highest_score": 0,
        "lowest_score": 0,
    }
    for fight in fights:
        hero_results = [result for result in fight["heroes"] if result["hero"]["_id"] == hero_id]
        if len(hero_results):
            hero_results = hero_results[0]
            hero_stats["fights"] += 1
            if fight["winner"]["hero"]["_id"] == hero_id:
                hero_stats["wins"] += 1
            else:
                hero_stats["losses"] += 1
            if hero_results["score"] > hero_stats["highest_score"]:
                hero_stats["highest_score"] = hero_results["score"]
            if hero_results["score"] < hero_stats["lowest_score"] or hero_stats["lowest_score"] == 0:
                hero_stats["lowest_score"] = hero_results["score"]
    return hero_stats
