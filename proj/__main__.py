from .app import APP


def main():
    """Main function for launching the Flask server."""
    APP.run("0.0.0.0")  # noqa: S104


if __name__ == "__main__":
    main()
