import json

from flask import Response

# Default error messages
MESSAGES = {
    "default": "An error has ocurred.",
    "404": "The requested resource or endpoint does not exist.",
    "405": "The method that you tried to use is not allowed for this endpoint.",
    "500": "The server was unable to complete the request due to an internal error."
}


def get_error_response(code: int, note=None):
    """Creates an flask.Response object for error codes."""

    # If a note was specified, use it
    if note is not None:
        message = note

    # If a error message is saved, use it
    elif str(code) in MESSAGES:
        message = MESSAGES[str(code)]

    # If nothing else works, just use a generic one
    else:
        message = MESSAGES["default"]

    # Finally, create a response and return it
    data = json.dumps({"status": code, "message": message})
    return Response(response=data, status=code, mimetype="application/json")
