import json

import flask
import werkzeug

from .flasked import Flasked
from .helpers import get_error_response
from .logic import do_fight, get_stats

HEROES_FIELDS = ["_id", "name", "description", "powers"]
APP = Flasked(__name__, static_folder=None)


@APP.errorhandler(werkzeug.exceptions.HTTPException)
def not_found(error):
    """Catch-All for HTTP error codes."""
    # flask/werkzeug thinks that a http error code is the same as an ImportError
    # if the error/exception has a code attribute, we guess that is a HTTPException
    if not hasattr(error, "code"):
        err_code = 500
    else:
        err_code = error.code

    # Finally, send the error back
    return get_error_response(err_code)


@APP.route("/heroes", methods=["GET"])
def get_heroes():
    """Returns every single hero from the database.

    Returned Heroes will be structured as follows:

    ```
    {
        "status": <HTTP status>,
        "message"?: "Detailed response message",
        "data": [{
            "_id": "Hero's unique ID",
            "name": "Hero's Name",
            "description": "Anything interesting about the hero or his/her/its backstory",
            "powers": [
                {
                    "name": "Name of the power",
                    "uses": "Number of times a power may be used in a single fight",
                    "strength": "Maximum possible strength of the power"
                }
            ]
        }]
    }
    """
    # This is "the easy endpoint", just fetch and return
    heroes = [x for x in APP.db["heroes"].find()]

    # But format what we need first
    data = json.dumps({"status": 200, "data": heroes})
    return flask.Response(response=data, status=200, mimetype="application/json")


@APP.route("/heroes/<int:herofetch>", methods=["GET"])
def get_hero(herofetch=None):
    """Returns a hero on the database.

    Returned Hero will be structured as follows:

    ```
    {
        "status": <HTTP status>,
        "message"?: "Detailed response message",
        "data": {
            "_id": "Hero's unique ID",
            "name": "Hero's Name",
            "description": "Anything interesting about the hero or his/her/its backstory",
            "powers": [
                {
                    "name": "Name of the power",
                    "uses": "Number of times a power may be used in a single fight",
                    "strength": "Maximum possible strength of the power"
                }
            ]
        }
    }
    ```
    """
    # If the ID was not specified, is a bad request
    if herofetch is None:
        return get_error_response(400, "The hero ID was not specified.")

    # Now we have an ID to fetch, so let's do that
    hero = APP.db["heroes"].find_one({"_id": herofetch})

    # Again, check that is really over there
    if hero is None:
        return get_error_response(404, "The requested hero does not exist.")

    # We have a hero to work with, let's create a response and return it
    data = json.dumps({"status": 200, "data": hero})
    resp = flask.Response(response=data, status=200, mimetype="application/json")
    return resp


@APP.route("/heroes/<int:herofetch>/stats", methods=["GET"])
def get_hero_stats(herofetch=None):
    """Returns stats for a hero on the database.

    Returned stats will be structured as follows:

    ```
    {
        "status": <HTTP status>,
        "message"?: "Detailed response message",
        "data": {
            "fights": <int>
            "wins": <int>
            "losses": <int>
            "highest_score": <int>
            "lowest_score": <int>
        }
    }
    ```
    """
    # If the ID was not specified, is a bad request
    if herofetch is None:
        return get_error_response(400, "The hero ID was not specified.")

    # Now we have an ID to fetch, so let's do that
    hero = APP.db["heroes"].find_one({"_id": herofetch})

    # Again, check that is really over there
    if hero is None:
        return get_error_response(404, "The requested hero does not exist.")

    # We have a hero to work with, let's pull their stats. Get all fights
    # where the heroes list contains a hero with the correct id.
    fights = APP.db["fights"].find({"heroes.hero._id": herofetch})

    # Then from that list of fights calculate the hero's stats
    stats = get_stats(fights, herofetch)

    data = json.dumps({"status": 200, "data": stats})
    resp = flask.Response(response=data, status=200, mimetype="application/json")
    return resp


@APP.route("/heroes", methods=["POST"])
def add_hero():
    """Adds a hero on the database.

    Hero should be structured as follows:

    ```
    {
        "name": "Hero's Name",
        "description": "Anything interesting about the hero or his/her/its backstory",
        "powers": [
            {
                "name": "Name of the power",
                "uses": "Number of times a power may be used in a single fight",
                "strength": "Maximum possible strength of the power"
            }
        ]
    }
    ```
    """
    # Maybe the user didn't sent the required parameters (or data at all)
    if "name" not in flask.request.json or "description" not in flask.request.json:
        return get_error_response(400, "The body is missing the name or description.")

    # First we check that the hero exists on the database
    hero = APP.db["heroes"].find_one({"name": flask.request.json["name"]})

    # We return if the dude already exists
    if hero is not None:
        # Throw an 409 Conflict if is already there
        return get_error_response(409, "The hero that you want to add already exists.")

    # Now we are sure that the hero does not exist, so let's add it
    new_hero = {
        "_id": APP.db["heroes"].count(),
        "name": flask.request.json["name"],
        "description": flask.request.json.get("description", "This hero does not have a description."),
        "powers": flask.request.json.get("powers", [])
    }
    APP.db["heroes"].insert_one(new_hero)

    # Nothing has crashed so far, we think that everything is OK
    data = json.dumps({
        "status": 201,
        "message": "The hero was added on the database.",
        "data": {"_id": new_hero["_id"]}
    })
    return flask.Response(response=data, status=201, mimetype="application/json")


@APP.route("/heroes/<int:herofetch>", methods=["PATCH"])
def edit_hero(herofetch=None):
    """Edit an existing hero.

    Hero should be structured as follows:

    ```
    {
        "name": "Hero's Name",
        "description": "Anything interesting about the hero or his/her/its backstory",
        "powers": [
            {
                "name": "Name of the power",
                "uses": "Number of times a power may be used in a single fight",
                "strength": "Maximum possible strength of the power"
            }
        ]
    }
    ```
    """
    # If the ID was not specified, is a bad request
    if herofetch is None:
        return get_error_response(400, "The hero ID was not specified.")

    # Try to get the hero from it's ID
    hero = APP.db["heroes"].find_one({"_id": herofetch})

    # If is not there, go back
    if hero is None:
        return get_error_response(404, "The hero that you want to edit does not exist.")

    # Now: the real work
    # First we check that the fields are correct
    for index, _item in flask.request.json.items():
        if index not in HEROES_FIELDS:
            return get_error_response(400, f"The field '{index}' is not valid.")

    # If they are, we update the document
    hero.update(flask.request.json)
    APP.db["heroes"].save(hero)

    # At this point everything should be OK
    data = json.dumps({"status": 200, "message": "The hero was edited.", "data": hero})
    return flask.Response(response=data, status=200, mimetype="application/json")


@APP.route("/fight", methods=["POST"])
def fight():
    """Given a list of combatants, calculates the results of the fight and stores
    the results in the database to be fetched at a later date.

    Expects:

    ```
    {
        "heroes": [
            "hero_id_1",
            "hero_id_2",
            "hero_id_3",
            ...
        ]
    }
    ```

    Returns:
    ```
    {
        "status": <HTTP status>,
        "message": "Detailed response message",
        "data": {
            "_id": fight_id
        }
    }

    ```
    """

    # Maybe the user didn't send a name (or data at all)
    if "heroes" not in flask.request.json:
        return get_error_response(404, "Expected a `heroes` property with a list of heroes")

    heroes = []
    for hero in flask.request.json["heroes"]:
        # First we check that the heroes each exist in the database
        hero_obj = APP.db["heroes"].find_one({"_id": hero})
        if hero_obj is None:
            return get_error_response(404, f"The hero {hero} does not exist.")
        heroes.append(hero_obj)

    # Now we are sure that the heroes all exist, let's generate the fight results!
    fight_results = do_fight(heroes)
    fight_results["_id"] = APP.db["fights"].count()
    APP.db["fights"].insert_one(fight_results)

    # Nothing has crashed so far, we think that everything is OK
    data = json.dumps({
        "status": 201,
        "message": "Fight scheduled, check back for the results!",
        "data": {"_id": fight_results["_id"]}
    })
    return flask.Response(response=data, status=201, mimetype="application/json")


@APP.route("/fight/<int:fightfetch>", methods=["GET"])
def get_fight(fightfetch=None):
    """Returns the Fight results for the specified fight.

    Returned Fight Results will be structured as follows:

    ```
    {
        "status": <HTTP status>,
        "message"?: "Detailed response message",
        "data": {
            "winner": An object representing the individual performance of the winner
            "heroes": A list representing the individual performance of each hero
            [{
                "hero": The Hero object
                "score": The final score for the Hero
                "best_power": The hero's most effective power this round
                {
                    "power": The Power object
                    "score": The amount of Score contributed by this power
                )
                "worst_power": The hero's least effective power this round
                (
                    "power": The Power object
                    "score": The amount of Score contributed by this power
                )
            }]
        }
    }
    ```
    """
    # If the ID was not specified, is a bad request
    if fightfetch is None:
        return get_error_response(400, "The fight ID was not specified.")

    # Now we have an ID to fetch, so let's do that
    fight = APP.db["fights"].find_one({"_id": fightfetch})

    # Again, check that is really over there
    if fight is None:
        return get_error_response(404, "The requested fight does not exist.")

    # We have a fight to work with, let's create a response and return it
    data = json.dumps({
        "status": 200,
        "data": fight
    })
    resp = flask.Response(response=data, status=200, mimetype="application/json")
    return resp


@APP.route("/", methods=["GET"])
def info():
    """Shows the available endpoints on the REST API."""
    # A place where we are going to save the endpoints
    endpoints = []

    # Iterate over the routes on the Flask APP
    for rule in APP.url_map.iter_rules():
        # Get the Options or Parameters
        options = {}
        for arg in rule.arguments:
            options[arg] = f"[{arg}]"

        # Save the endpoint
        formated = {"url": str(rule), "methods": [str(x) for x in rule.methods]}

        # And add the docstring if is there
        if rule.endpoint in globals():
            formated["docstring"] = globals()[rule.endpoint].__doc__

        # Finally create a list and do wherever uou want
        endpoints.append(formated)

    # Create a response object and return it as code 200
    data = json.dumps({"status": 200, "message": "The following endpoints are available:", "data": endpoints})
    resp = flask.Response(response=data, status=200, mimetype="application/json")
    return resp
