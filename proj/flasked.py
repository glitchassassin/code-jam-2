from flask import Flask
from pymongo import MongoClient

from .config import get_config


class Flasked(Flask):
    """Variant of flask.Flask with built-in MongoDB support."""
    def __init__(self, *args, **kwargs):
        # Get our configuration
        self.apiconfig = get_config()
        # And create a MongoDB instance
        self.mongo = MongoClient(self.apiconfig["mongourl"])
        # With the database
        self.db = self.mongo[self.apiconfig["database"]]

        # Once everything is OK, continue with the Flask workflow
        super().__init__(*args, **kwargs)
