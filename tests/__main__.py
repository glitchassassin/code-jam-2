from pprint import pprint

import requests


TARGET_SERVER = "127.0.0.1"
TARGET_PORT = "5000"
BASE_URL = f"http://{TARGET_SERVER}:{TARGET_PORT}"


def main():
    # Reset Mongo DB
    # Call / to get info
    print("Testing info endpoint...")
    response = requests.get(f"{BASE_URL}/").json()
    assert(response["status"] == 200)
    assert(response["message"] == "The following endpoints are available:")
    assert(len([e for e in response["data"] if e["url"] == "/heroes"]) > 0)

    # Call /heroes/ to confirm it is empty
    print("Making sure /heroes/ is empty...")
    response = requests.get(f"{BASE_URL}/heroes").json()
    assert(response["status"] == 200)
    assert(len(response["data"]) == 0)

    # Post a new hero to /heroes
    print("Creating new hero")
    response = requests.post(f"{BASE_URL}/heroes", json={
        "name": "Hercules",
        "description": "Strongest demigod alive!",
        "powers": [
            {
                "name": "Divine Punch",
                "uses": 1,
                "strength": 100
            },
            {
                "name": "Shovel Furiously",
                "uses": 30,
                "strength": 30
            },
        ]
    }).json()
    assert(response["status"] == 201)
    assert(response["message"] == "The hero was added on the database.")
    hercules_id = response["data"]["_id"]

    # Call /heroes/<id> to fetch created hero
    print("Fetching created hero")
    response = requests.get(f"{BASE_URL}/heroes/{hercules_id}").json()
    assert(response["status"] == 200)
    assert(response["data"]["name"] == "Hercules")
    pprint(response["data"])

    # Post a new hero to /heroes
    print("Adding a second hero")
    response = requests.post(f"{BASE_URL}/heroes", json={
        "name": "Zeus",
        "description": "The other god of thunder",
        "powers": [
            {
                "name": "Lightning Bolt",
                "uses": 4,
                "strength": 100
            },
            {
                "name": "Getting Busy",
                "uses": 30,
                "strength": 2
            },
        ]
    }).json()
    assert(response["status"] == 201)
    assert(response["message"] == "The hero was added on the database.")
    zeus_id = response["data"]["_id"]

    # Patch the original hero in /heroes/<id>
    print("Updating Hercules")
    response = requests.patch(f"{BASE_URL}/heroes/{hercules_id}", json={
        "name": "Hercules",
        "description": "Strongest demigod alive! Maybe as strong as the Hulk?",
        "powers": [
            {
                "name": "Divine Punch",
                "uses": 1,
                "strength": 100
            },
            {
                "name": "Shovel Furiously",
                "uses": 30,
                "strength": 10
            },
            {
                "name": "Hurl Boulders",
                "uses": 3,
                "strength": 30
            },
        ]
    }).json()
    assert(response["status"] == 200)
    assert(response["message"] == "The hero was edited.")

    # Call /heroes/ to confirm that both heroes appear
    print("Listing all heroes")
    response = requests.get(f"{BASE_URL}/heroes").json()
    assert(response["status"] == 200)
    assert(len(response["data"]) == 2)
    assert(len([hero for hero in response["data"] if hero["name"] == "Hercules"]) == 1)
    assert(len([hero for hero in response["data"] if hero["name"] == "Zeus"]) == 1)
    pprint(response["data"])

    # Post a fight roster to /fight/
    print("Posting a fight roster")
    response = requests.post(f"{BASE_URL}/fight", json={
        "heroes": [
            hercules_id,
            zeus_id
        ]
    }).json()
    assert(response["status"] == 201)
    assert(response["message"] == "Fight scheduled, check back for the results!")
    assert("_id" in response["data"])
    fight_results = response["data"]["_id"]

    # Call /fight/<id> to fetch the fight results
    print("Getting the results of posted fight roster")
    response = requests.get(f"{BASE_URL}/fight/{fight_results}").json()
    assert(response["status"] == 200)
    assert("winner" in response["data"])
    pprint(response["data"])

    # Post a new fight roster to /fight/
    print("Posting another fight roster")
    response = requests.post(f"{BASE_URL}/fight", json={
        "heroes": [
            hercules_id,
            zeus_id
        ]
    }).json()
    assert(response["status"] == 201)
    assert(response["message"] == "Fight scheduled, check back for the results!")
    assert("_id" in response["data"])
    fight_results_2 = response["data"]["_id"]

    # Call /fight/<id> to fetch the fight results
    print("Getting the results of the second fight")
    response = requests.get(f"{BASE_URL}/fight/{fight_results_2}").json()
    assert(response["status"] == 200)
    assert("winner" in response["data"])
    pprint(response["data"])

    # Call /heroes/<id>/stats to fetch the hero's fight stats
    print("Getting Hercules' fight stats")
    response = requests.get(f"{BASE_URL}/heroes/{hercules_id}/stats").json()
    assert(response["status"] == 200)
    assert("wins" in response["data"])
    pprint(response["data"])


if __name__ == "__main__":
    main()
